provider "aws" {
  region = "us-east-1"
}
module "autoscale-group" {
  source = "git::ssh://git@github.azc.ext.hp.com/MSIT-DevOps/TerraformModules.git//autoscale-group?ref=master-14"

  # Launch configuration
  app_name = "${var.app_name}"
  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"
  user_data = "${data.template_cloudinit_config.init.rendered}"
  image_id = "${data.aws_ami.ecs_optimized.id}"
  instance_type = "t2.micro"
  key_name = "${var.key_name}"
  iam_instance_profile = "${module.ecs-cluster.instance_profile_id}"

  root_block_device {
    volume_type = "gp2"
    volume_size = "50"
    iops = "${var.root_block_device_iops}"
    delete_on_termination = "true"
  }

  lifecycle {
    create_before_destroy = true
  }

# Auto Scaling Group
vpc_zone_identifier = "${data.terraform_remote_state.vpc.private_subnet_ids}"
min_size = "2"
max_size = "5"
health_check_type = "EC2"
health_check_grace_period = "${var.health_check_grace_period}"

tags = [
  {
    key                 = "Environment"
    value               = "dev"
    propagate_at_launch = true
  },
  {
    key                 = "Project"
    value               = "megasecret"
    propagate_at_launch = true
  },
]

}