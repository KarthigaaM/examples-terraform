
resource "aws_cloudwatch_metric_alarm" "MemoryUtilization" {
  count = 1
  alarm_name = "kar-ECS-Cluster-MemoryUtilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = 10
  metric_name = "MemoryUtilization"
  namespace = "AWS/ECS"
  period = 60
  statistic = "Average"
  threshold = 80
  alarm_description = "This metric monitors Memory Utilization for an ECS Cluster"


}