variable "allocated_storage" {default = 10}
variable "storage_type" {default = "gp2"}
variable "engine" {default = "mysql"}
variable "engine_version" {default = 5.7}
variable "instance_class" {default = "db.t2.micro"}
variable "name" {default = "samp"}
variable "parameter_group_name" {default = "default.mysql5.7"}
